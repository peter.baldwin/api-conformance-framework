## Getting Started
1. create test cases in JSON format and put the resulting file in the "input" folder
2. implement (in auth.utils.ts file) the process for getting an Authorization token (if required)
3. install dependencies (ie. invoke `npm install`)
4. invoke `npm test` to execute the test cases

### Process for Creating a Test
    1. identify the data for the test: details of API endpoint (eg. path, headers); expected response status code; (optional) REGEX of values in response payload
    2. identify the base URL of the endpoints (ie. the name of the host/server)
    3. enter the tests' data in the file that is in the "input" folder
    4. enter the endpoints' base URL in the "input/env/environment_configuration.json" file

## Design Notes
* This test framework is implemented in the "__tests__/API.framework.ts" file
* Scope of Framework: this test framework is intended to verify the signature of API endpoints. The following tests of API responses are performed:
    1. check that the Response status code is the expected value
    2. check that the Response payload is conformant with the nominated OPENAPI schema (ie YAML file)
    3. check that the value of a nominated JSON attribute (in the response payload) is compliant with the provided REGEX
* Any data setup that is required to support the test cases should be done independently of the execution of this API Conformance test framework.
