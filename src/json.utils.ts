import {v1 as uuidv1} from 'uuid'

var moment    = require('moment')
let timestamp  = new Date()

export class JsonUtils {
    replaceHandlebarParameters(json: any): any {
        for (var key in json) {

            if (typeof(json[key]) == "object") {
                this.replaceHandlebarParameters(json[key])

            } else {
                if ((json[key]).match("\{\{guid\}\}")) {
                    json[key] = `${uuidv1()}`;
                } else if ((json[key]).match("\{\{timestamp\}\}")) {
                    json[key] = `${moment(timestamp).format('YYYY-MM-DDTHH:mm:ssZ')}`;
                }
            }
        }

        return json
    }
}
