import { JSONPath }     from 'jsonpath-plus';
import   Request        from 'supertest';
import jestOpenAPI from 'jest-openapi'

import testsJson from '../input/tests_fixture.json';
import envJson from '../input/env/environment_configuration.json'
let environmentConfig: any

import { AuthUtils } from '../src/auth.utils'
let auth_utils = new AuthUtils()
import { JsonUtils } from '../src/json.utils'
let json_utils = new JsonUtils()

const CHAR_NOT_FOUND = -1
let OAuthtoken: string

jestOpenAPI('/Users/pbaldwin/Workspace/interop-api/api-gateway/api_spec.yml')


beforeAll(async () => {
    OAuthtoken = await auth_utils.retrieveAUTHToken()

    environmentConfig = JSONPath({path: `$..[?(@.name =="JITLR")]`, json: envJson})
})

describe("IOP API contract testing", () => {

    const executeMethod =
    async (test_config: any) => {
        let base    = JSONPath({path: "$..base_URL" , json: environmentConfig}).toString()

        let method  = JSONPath({path: "$..method", json: test_config}).toString()
        let path    = JSONPath({path: "$..path", json: test_config})
        let payload = JSONPath({path: "$..body.raw", json: test_config})

        let endpointFullPath = JSONPath({path: "$..url.raw", json: test_config})
        let endpointQuery: string    = (endpointFullPath.toString().indexOf("?") == CHAR_NOT_FOUND ?
                                     null :
                                     endpointFullPath.toString().substring(endpointFullPath.toString().indexOf('?') + 1))
        let expectedResponseCode = JSONPath({path: "$..status", json: test_config})

        switch (method) {
            case "GET":
                return await Request(base).get(path).query(endpointQuery).set(constructHeaders(test_config)).expect(expectedResponseCode)
            case "POST":
                return await Request(base).post(path).send(json_utils.replaceHandlebarParameters(payload)).set(constructHeaders(test_config)).expect(expectedResponseCode)
            case "PUT":
                return await Request(base).put(path).send(json_utils.replaceHandlebarParameters(payload)).set(constructHeaders(test_config)).expect(expectedResponseCode)
        }

    }

    const constructHeaders =
    (configJSON: any): object => {
        let requiredHeaders = JSONPath({path: "$..header.*", json: configJSON})

        let retObject: {[headerName: string]: string} = {}

        for (let index = 0; index < requiredHeaders.length; index++) {
            let obj = requiredHeaders[index]
            if (obj['value'].match("\{\{access_token\}\}")) {
                retObject[obj['key']] = OAuthtoken
            } else {
                retObject[obj['key']] = obj['value']
            }
        }

        return json_utils.replaceHandlebarParameters(retObject)
    }

    const verifyAssertions =
    (regexAssertions: any[], responseBody: any) => {

        for (let index = 0; index < regexAssertions.length; index++) {
            let obj = regexAssertions[index]
            for (var key in obj){
                if (key === "status") {
                    continue
                } else {
                    let actualValue = JSONPath({path: `$..${key}`, json: responseBody})
                    expect(actualValue[0]).toMatch(new RegExp(obj[key]))
                }
            }
        }
    }

    const testCallback =
    ({...config}) =>
    async () => {

        let response = await executeMethod(config)

        expect(response).toSatisfyApiSpec()

        verifyAssertions(JSONPath({path: "$..assertion.*", json: config}), response.body)
    }


    let tests = JSONPath({path: "$..name", json: testsJson})
    tests.forEach((name: any) => {
        let testDetails = JSONPath({path: `$..[?(@.name =="${name}")]`, json: testsJson})
        it(name, testCallback({testDetails}))
    })
})